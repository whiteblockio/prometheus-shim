FROM prom/prometheus:v2.5.0

COPY prometheus.yml /etc/prometheus/prometheus.yml

CMD [ \
  "--log.level=debug", \
  "--config.file=/etc/prometheus/prometheus.yml", \
  "--storage.tsdb.path=/prometheus", \
  "--web.console.libraries=/etc/prometheus/console_libraries", \
  "--web.console.templates=/etc/prometheus/consoles" \
]
